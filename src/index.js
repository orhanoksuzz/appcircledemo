// Intro.js
import React, {Component} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';

class Intro extends Component {
    state={
        text:''
    }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>
          This is a React Native snapshot test. edit edit 2
        </Text>
        <View
          style={{
            height: 50,
            width: 200,
            alignSelf: 'center',
            marginTop: 50,
            borderWidth: 1,
          }}>
          <TextInput
            style={{flexGrow: 1}}
            onChangeText={(text) => this.setState({text})}
            value={this.state.text}
            testID='text'
          />
        </View>
        <View style={{marginTop:25,height:50,width:200,alignSelf:'center',borderWidth:1}} >
          <Button title="kaydet" onPress={() => alert(this.state.text)} testID='save' />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    flex: 1,
    justifyContent: 'center',
  },
  instructions: {
    color: '#333333',
    marginBottom: 5,
    textAlign: 'center',
  },
  welcome: {
    fontSize: 20,
    margin: 10,
    textAlign: 'center',
  },
});

export default Intro;
